import QtQuick 2.0

Item {
    id: idContainer

    property int swipeHorizontal: 0
    property int swipeVertical: 0
    property int swipeThreshhold: width * 0.1
    property bool swiping: false
    property bool swipingHorizontaly: false
    property bool swipingVerticaly: false
    property int alertThreshhold: width * 0.45
    property int swipeBounds: width * 0.5
    property bool swipeEnabled: true

    signal swipedLeft
    signal swipedUp
    signal swipedRight
    signal swipedDown
    signal clicked

    QtObject {
        id: idPrivate
        property bool held: false
        property int clickX: 0
        property int clickY: 0
        property int deltaX: 0
        property int deltaY: 0
        property bool canSwipeHor: true
        property bool canSwipeVer: true
        property bool possibleClick: true
    }

    MouseArea {
        id: idMouseArea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton

        onPressed: {
            if (idContainer.swipeEnabled) {
                if (mouse.button == Qt.LeftButton) {
                    idPrivate.held = true
                    idPrivate.clickX = mouse.x
                    idPrivate.clickY = mouse.y
                    idPrivate.deltaX = 0
                    idPrivate.deltaY = 0
                    idContainer.swiping = false
                    idContainer.swipingHorizontaly = false
                    idContainer.swipingVerticaly = false
                    idPrivate.canSwipeHor = true
                    idPrivate.canSwipeVer = true
                }
            } else {
                mouse.accepted = false
            }
        }

        onMouseXChanged: {
            if (idContainer.swipeEnabled) {
                var deltaX = mouse.x - idPrivate.clickX
                if (idPrivate.canSwipeHor
                        && Math.abs(deltaX) > Math.abs(idPrivate.deltaY)
                        && Math.abs(deltaX) > idContainer.swipeThreshhold) {
                    if (deltaX > idContainer.swipeBounds) {
                        deltaX = idContainer.swipeBounds
                    } else if (deltaX < -idContainer.swipeBounds) {
                        deltaX = -idContainer.swipeBounds
                    }

                    idPrivate.possibleClick = false
                    idContainer.swipeHorizontal = idPrivate.deltaX
                    idContainer.swipingHorizontaly = true
                    idContainer.swiping = true
                    idContainer.swipingVerticaly = false
                    idPrivate.canSwipeVer = false
                } else {
                    idContainer.swipingHorizontaly = false
                }
                idPrivate.deltaX = deltaX
            } else {
                mouse.accepted = false
            }
        }

        onMouseYChanged: {
            if (idContainer.swipeEnabled) {
                var deltaY = mouse.y - idPrivate.clickY
                if (idPrivate.canSwipeVer
                        && Math.abs(deltaY) > Math.abs(idPrivate.deltaX)
                        && Math.abs(deltaY) > idContainer.swipeThreshhold) {
                    if (deltaY > idContainer.swipeBounds) {
                        deltaY = idContainer.swipeBounds
                    } else if (deltaY < -idContainer.swipeBounds) {
                        deltaY = -idContainer.swipeBounds
                    }

                    idPrivate.possibleClick = false
                    idContainer.swipeVertical = idPrivate.deltaY
                    idContainer.swipingVerticaly = true
                    idContainer.swiping = true
                    idContainer.swipingHorizontaly = false
                    idPrivate.canSwipeHor = false
                } else {
                    idContainer.swipingVerticaly = false
                }
                idPrivate.deltaY = deltaY
            } else {
                mouse.accepted = false
            }
        }

        onReleased: {
            if (idContainer.swipeEnabled) {
                if (mouse.button == Qt.LeftButton) {
                    if (idContainer.swipingHorizontaly) {
                        if (idContainer.swipeHorizontal < -idContainer.alertThreshhold) {
                            idContainer.swipedLeft()
                        } else if (idContainer.swipeHorizontal > idContainer.alertThreshhold) {
                            idContainer.swipedRight()
                        }
                    } else if (idContainer.swipingVerticaly) {
                        if (idContainer.swipeVertical < -idContainer.alertThreshhold) {
                            idContainer.swipedUp()
                        } else if (idContainer.swipeVertical > idContainer.alertThreshhold) {
                            idContainer.swipedDown()
                        }
                    }
                    if (idPrivate.possibleClick) {
                        idContainer.clicked()
                    }
                    idPrivate.possibleClick = true
                    idPrivate.held = false
                    idPrivate.deltaX = 0
                    idPrivate.deltaY = 0
                    idContainer.swipingHorizontaly = false
                    idContainer.swipingVerticaly = false
                    idContainer.swiping = false
                }
            } else {
                mouse.accepted = false
            }
        }
    }
}

