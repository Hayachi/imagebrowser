/* QtQuick 2.0

Item {
    id: idFullscreen
    property alias source: idImage.source

    Rectangle {
        color: "#606060"
        anchors.fill: parent
        LoadableImage {
            id: idImage
            anchors.fill: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (mouse.button == Qt.LeftButton) {
                    idFullscreen.visible = false
                    idScroll.visible = true
                }
            }

        }
    }

    InteractiveImage {
        id: idImage
        anchors.fill: parent
    }
}*/

import QtQuick 2.0

Item {
    id: idContainer
    clip: true

    signal requestSave
    signal requestRemove
    signal requestNext
    signal requestPrevious
    signal clicked

    property bool interactive: false
    property alias source: idDraggable.source

    Rectangle {
        height: parent.height - parent.width * 0.25
        width: parent.width / 8
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        color: "#50C050"

        Image {
            anchors.fill: parent
            scale: 0.5
            source: "/res/save-icon.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Rectangle {
        height: parent.height - parent.width * 0.25
        width: parent.width / 8
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        color: "#C05050"

        Image {
            anchors.fill: parent
            scale: 0.5
            source: "/res/trash-.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    SwipeArea {
        id: idMouseArea
        anchors.fill: parent
        swipeThreshhold: 5
        swipeBounds: width * 0.125
        alertThreshhold: width * 0.12
        onSwipingChanged: {
            idContainer.interactive = swiping
            if (!idContainer.interactive) {
                swipeEnabled = false
                idDraggable.moveBack()
            }
        }
        onSwipeHorizontalChanged: {
            idDraggable.x = swipeHorizontal
        }
        onSwipeVerticalChanged: {
            idDraggable.y = swipeVertical
        }

        onSwipedLeft: {
            idContainer.interactive = false
            idContainer.requestSave()
        }
        onSwipedRight: {
            idContainer.interactive = false
            idContainer.requestRemove()
        }
        onSwipedUp: {
            idContainer.interactive = false
            idContainer.requestNext()
        }
        onSwipedDown: {
            idContainer.interactive = false
            idContainer.requestPrevious()
        }

        onClicked: {
            idContainer.clicked()
        }
    }

    DraggableImage {
        id: idDraggable
        width: parent.width
        height: parent.height
        onReturned: {
            idMouseArea.swipeEnabled = true
        }
    }
}
