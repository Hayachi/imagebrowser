import QtQuick 2.4
import QtQuick.Controls 1.3
import ImageBrowser 1.0

Item {
    id: idContainer
    property alias address: idImageProvider.address
    property alias regexp: idImageProvider.regexp
    property alias columns: idImageProvider.columns
    property alias imagesCount: idImageProvider.imagesCount
    property alias maxImagesCount: idImageProvider.maxImagesCount
    property alias capGroup: idImageProvider.capGroup
    property alias thumbGroup: idImageProvider.thumbGroup

    signal requestNew

    function loadCache() {
        idImageProvider.loadCache();
    }

    function popCurrent() {
        idImageProvider.popCurrent()
        idScroll.contentX = 0
        idScroll.contentY = 0
    }

    function clearCache() {
        idImageProvider.clearCache()
    }

    function reload() {
        idImageProvider.reload()
    }

    ImageProvider {
        id: idImageProvider
        visible: false
        onHasImagesChanged: {
            if (hasImages) {
                idPlaceHolder.visible = false
                idScroll.visible = true
            } else {
                idPlaceHolder.visible = true
                idScroll.visible = false
            }
        }
    }

    Item {
        id: idPlaceHolder
        anchors.fill: parent

        Text {
            anchors.centerIn: parent
            text: "No images"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                idContainer.requestNew()
            }
        }
    }

    FullscreenDisplay {
        id: idFullscreen
        anchors.fill: parent
        visible: false
        property int currentIndex: 0

        function changeIndex(id) {
            var newIndex = id
            if (newIndex < 0) {
                newIndex = 0
            }
            if (newIndex >= idImageProvider.imagesFound) {
                newIndex = idImageProvider.imagesFound - 1
            }
            if (newIndex < 0) {
                visible = false
                idPlaceHolder.visible = true
                idScroll.visible = false
            } else {
                currentIndex = newIndex
                source = idImageProvider.getImage(newIndex)
            }
        }

        onRequestNext: {
            changeIndex(currentIndex + 1)
        }

        onRequestPrevious: {
            changeIndex(currentIndex - 1)
        }

        onRequestRemove: {
            idImageProvider.remove(currentIndex)
            changeIndex(currentIndex)

        }
        onRequestSave: {
            idImageProvider.save(currentIndex)
            changeIndex(currentIndex)
        }

        onClicked: {
            visible = false
            idScroll.visible = idImageProvider.hasImages
            idPlaceHolder.visible = !idImageProvider.hasImages
        }
    }

    /*Item {
        id: idFullscreen
        anchors.fill: parent
        visible: false

        Rectangle {
            color: "#333333"
            anchors.fill: parent
            Image {
                id: idImgFullscreen
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit

            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (mouse.button == Qt.LeftButton) {
                        idFullscreen.visible = false
                        idScroll.visible = true
                    }
                }

            }
        }
    }*/

    Flickable {
        id: idScroll
        anchors.fill: parent
        anchors.margins: 3
        contentHeight: idGrid.height
        contentWidth: idGrid.width
        flickableDirection: Flickable.VerticalFlick
        clip: true
        visible: idImageProvider.hasImages

        Grid {
            id: idGrid
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 3
            columns: idImageProvider.columns

            Repeater {
                id: idRepeater
                model: idImageProvider.imagesCount

                InteractiveImage {
                    width: (idContainer.width - (idGrid.columns + 1) * 3) / idGrid.columns
                    height: width

                    source: idImageProvider.getThumb(index)

                    onRequestRemove: {
                        idImageProvider.remove(index)
                    }

                    onRequestSave: {
                        idImageProvider.save(index)
                    }

                    onInteractiveChanged: {
                        idScroll.interactive = !interactive
                    }

                    onClicked: {
                        idFullscreen.source = idImageProvider.getImage(index)
                        idFullscreen.currentIndex = index
                        idFullscreen.visible = true
                        idScroll.visible = false
                    }
                }
            }
        }
    }
}
