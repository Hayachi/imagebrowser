import QtQuick 2.0

Item {
    property alias source: idImage.source

    Image {
        id: idLoading
        anchors.fill: parent
        source: "/res/IconTime.png"
        fillMode: Image.PreserveAspectFit
        scale: 0.5
    }

    Image {
        id: idError
        anchors.fill: parent
        source: "/res/error.png"
        fillMode: Image.PreserveAspectFit
        scale: 0.5
        visible: false
    }

    Image {
        id: idImage
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        asynchronous: true
        visible: false
        onStatusChanged: {
            if (status == Image.Ready) {
                idLoading.visible = false
                idImage.visible = true
                idError.visible = false
            } else if (status == Image.Loading) {
                idImage.visible = false
                idLoading.visible = true
                idError.visible = false
            } else if (status == Image.Error) {
                idImage.visible = false
                idLoading.visible = false
                idError.visible = true
            }
        }
    }
}

