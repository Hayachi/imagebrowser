import QtQuick 2.0

Item {
    id: idDraggable
    property alias source: idImage.source
    signal returned

    function moveBack() {
        idBackAnimHorizontal.restart()
        idBackAnimVertical.restart()
    }

    NumberAnimation on x {
        id: idBackAnimHorizontal
        to: 0
        onStopped: {
            if (idBackAnimVertical.running == false) {
                idDraggable.returned()
            }
        }
    }

    NumberAnimation on y {
        id: idBackAnimVertical
        to: 0
        onStopped: {
            if (idBackAnimHorizontal.running == false) {
                idDraggable.returned()
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#606060"
        LoadableImage {
            id: idImage
            anchors.fill: parent
            anchors.margins: 2
        }
    }
}
