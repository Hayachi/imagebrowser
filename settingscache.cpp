#include "settingscache.h"
#include <QStandardPaths>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>

SettingsCache::SettingsCache()
{
    m_cacheFile = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    m_cacheFile += "/ImageBrowser";
    QDir().mkpath(m_cacheFile);
    m_cacheFile += "/settings.jsn";
    reload();
}

SettingsCache::~SettingsCache()
{

}

void SettingsCache::saveCache()
{
    QFile file(m_cacheFile);
    if (file.open(QFile::WriteOnly)) {
        QJsonObject obj;
        obj["filters"] = m_filters;
        obj["columnsCount"] = m_columnsCount;
        obj["itemsCount"] = m_itemsCount;
        obj["lastAddress"] = m_lastAddress;
        obj["lastName"] = m_lastName;
        obj["lastRegex"] = m_lastRegex;
        obj["lastCapGroup"] = m_lastCapGroup;
        obj["thumbnailGroup"] = m_thumbnailGroup;
        file.write(QJsonDocument(obj).toJson());
        file.close();
        qDebug() << "+Settings cache saved";
    } else {
        qDebug() << "--Failed to save settings cache";
    }
}

void SettingsCache::saveCurrent()
{
    QJsonObject obj;
    obj["regex"] = lastRegex();
    obj["capgroup"] = lastCapGroup();
    obj["name"] = lastName();
    obj["thumbnailGroup"] = thumbnailGroup();
    m_filters.push_back(obj);
    emit(filtersCountChanged());
}

void SettingsCache::reload()
{
    QFile file(m_cacheFile);
    if (file.open(QFile::ReadOnly)) {
        QJsonObject obj = QJsonDocument::fromJson(file.readAll()).object();
        file.close();
        m_filters = obj["filters"].toArray();
        setItemsCount(obj["itemsCount"].toInt());
        setColumnsCount(obj["columnsCount"].toInt());
        setLastAddress(obj["lastAddress"].toString());
        setLastCapGroup(obj["lastCapGroup"].toInt());
        setLastName(obj["lastName"].toString());
        setLastRegex(obj["lastRegex"].toString());
        setThumbnailGroup(obj["thumbnailGroup"].toInt());
        if (lastRegex() == "") {
            setDefaultSettings();
        }
        qDebug() << "+Settings cache read";
    } else {
        setDefaultSettings();
        setDefaultFilters();
        saveCache();
        reload();
    }
    emit filtersCountChanged();
}

QString SettingsCache::patternAt(int id)
{
    if (id >= m_filters.size() || id < 0) {
        return QString("");
    }
    return m_filters[id].toObject()["regex"].toString();
}

int SettingsCache::thumbnailGroupAt(int id)
{
    if (id >= m_filters.size() || id < 0) {
        return 2;
    }
    return m_filters[id].toObject()["thumbnailGroup"].toInt();
}

int SettingsCache::capGroupAt(int id)
{
    if (id >= m_filters.size() || id < 0) {
        return 2;
    }
    return m_filters[id].toObject()["capgroup"].toInt();
}

QString SettingsCache::nameAt(int id)
{
    if (id >= m_filters.size() || id < 0) {
        return QString("Unnamed");
    }
    return m_filters[id].toObject()["name"].toString();
}

void SettingsCache::remove(int id)
{
    if (id >= filtersCount() || id < 0) {
        return;
    }
    m_filters.removeAt(id);
    emit filtersCountChanged();
}

void SettingsCache::set(int id)
{
    if (id >= filtersCount() || id < 0) {
        return;
    }
    setLastRegex(patternAt(id));
    setLastName(nameAt(id));
    setLastCapGroup(capGroupAt(id));
    setThumbnailGroup(thumbnailGroupAt(id));
}

void SettingsCache::setDefaultFilters()
{
    QFile defSett(":/res/default/settings.jsn");
    if (defSett.open(QFile::ReadOnly)) {
        QJsonObject obj = QJsonDocument::fromJson(defSett.readAll()).object();
        defSett.close();
        m_filters = obj["filters"].toArray();
        emit filtersCountChanged();
        qDebug() << "+Default filters set";
    } else {
        qDebug() << "--Failed to open the default settings file";
    }
}

void SettingsCache::setDefaultSettings()
{
    QFile defSett(":/res/default/settings.jsn");
    if (defSett.open(QFile::ReadOnly)) {
        QJsonObject obj = QJsonDocument::fromJson(defSett.readAll()).object();
        defSett.close();
        setItemsCount(obj["itemsCount"].toInt());
        setColumnsCount(obj["columnsCount"].toInt());
        setLastAddress(obj["lastAddress"].toString());
        setLastCapGroup(obj["lastCapGroup"].toInt());
        setLastName(obj["lastName"].toString());
        setLastRegex(obj["lastRegex"].toString());
        setThumbnailGroup(obj["thumbnailGroup"].toInt());
        saveCache();
        qDebug() << "+Default settings set";
    } else {
        qDebug() << "--Failed to open the default settings file";
    }
}

void SettingsCache::clearFilters()
{
    m_filters = QJsonArray();
    setDefaultFilters();
    saveCache();
}

void SettingsCache::clearSettings()
{
    setDefaultSettings();
    saveCache();
}

int SettingsCache::columnsCount() const
{
    return m_columnsCount;
}

void SettingsCache::setColumnsCount(int c)
{
    m_columnsCount = c;
    emit columnsCountChanged();
}

int SettingsCache::itemsCount() const
{
    return m_itemsCount;
}

void SettingsCache::setItemsCount(int c)
{
    m_itemsCount = c;
    emit itemsCountChanged();
}

QString SettingsCache::lastAddress() const
{
    return m_lastAddress;
}

void SettingsCache::setLastAddress(QString s)
{
    m_lastAddress = s;
    emit lastAddressChanged();
}

int SettingsCache::filtersCount() const
{
    return m_filters.size();
}

QString SettingsCache::lastRegex() const
{
    return m_lastRegex;
}

void SettingsCache::setLastRegex(QString s)
{
    m_lastRegex = s;
    emit lastRegexChanged();
}

int SettingsCache::lastCapGroup() const
{
    return m_lastCapGroup;
}

void SettingsCache::setLastCapGroup(int g)
{
    m_lastCapGroup = g;
    emit lastCapGroupChanged();
}

QString SettingsCache::lastName() const
{
    return m_lastName;
}

void SettingsCache::setLastName(QString s)
{
    m_lastName = s;
    emit lastNameChanged();
}

int SettingsCache::thumbnailGroup() const
{
    return m_thumbnailGroup;
}

void SettingsCache::setThumbnailGroup(int g)
{
    m_thumbnailGroup = g;
    emit thumbnailGroupChanged();
}

