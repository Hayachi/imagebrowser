#ifndef IMAGEPROVIDER_H
#define IMAGEPROVIDER_H

#include <QQuickItem>
#include <QDir>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

class ImageProvider : public QQuickItem
{
    Q_OBJECT
public:
    ImageProvider();
    ~ImageProvider();
    Q_INVOKABLE QString getImage(int) const;
    Q_INVOKABLE QString getThumb(int) const;
    Q_INVOKABLE void remove(int);
    Q_INVOKABLE void save(int);
    Q_INVOKABLE void popCurrent();
    Q_INVOKABLE void clearCache();
    Q_INVOKABLE void saveCache();
    Q_INVOKABLE void loadCache();
    Q_INVOKABLE void reload();

    Q_PROPERTY(int columns READ columns WRITE setColumns NOTIFY columnsChanged)
    Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(QString regexp READ regexp WRITE setRegexp NOTIFY regexpChanged)
    Q_PROPERTY(int imagesCount READ imagesCount WRITE setImagesCount NOTIFY imagesCountChanged)
    Q_PROPERTY(int maxImagesCount READ maxImagesCount WRITE setMaxImagesCount NOTIFY maxImagesCountChanged)
    Q_PROPERTY(bool hasImages READ hasImages NOTIFY hasImagesChanged)
    Q_PROPERTY(int capGroup READ capGroup WRITE setCapGroup NOTIFY capGroupChanged)
    Q_PROPERTY(int thumbGroup READ thumbGroup WRITE setThumbGroup NOTIFY thumbGroupChanged)
    Q_PROPERTY(int imagesFound READ imagesFound NOTIFY imagesFoundChanged)

    int columns() const;
    void setColumns(int);
    int totalItems() const;
    void setTotalItems(int);
    QString address() const;
    void setAddress(QString);
    QString regexp() const;
    void setRegexp(QString);
    int imagesCount() const;
    void setImagesCount(int);
    int maxImagesCount() const;
    void setMaxImagesCount(int);
    bool hasImages() const;
    int capGroup() const;
    void setCapGroup(int);
    int thumbGroup() const;
    void setThumbGroup(int);
    int imagesFound() const;

signals:
    void columnsChanged();
    void totalItemsChanged();
    void addressChanged();
    void regexpChanged();
    void imagesCountChanged();
    void maxImagesCountChanged();
    void hasImagesChanged();
    void capGroupChanged();
    void thumbGroupChanged();
    void imagesFoundChanged();

public slots:

private slots:
    void parseReply();
    void parseImage();

private:
    void initialize();
    void tryPush(QString, QString th);
    void pareseImageReply(QPair<QNetworkReply *, QString> r);
    QString fixUrl(QString);

    QVector<QPair<QString, QString>> m_images;
    QString m_address;
    QString m_regexp;
    QNetworkAccessManager m_manager;
    QNetworkReply *m_scanReply;
    QVector<QPair<QNetworkReply*, QString>> m_imageReplies;
    QJsonObject m_records;
    int m_columns;
    int m_imagesCount;
    int m_maxImagesCount;
    int m_capGroup;
    int m_unknownImages;
    int m_totalFound;
    int m_thumbGroup;
};

#endif // IMAGEPROVIDER_H
