import QtQuick 2.0
import QtQuick.Controls 1.3

MenuBar {
    id: idMenuBar
    signal showSettings
    signal dumpCurrent
    signal clearCache
    signal quit

    Menu {
        title: "Settings"
        MenuItem {
            text: "Select"
            onTriggered: {
                idMenuBar.showSettings()
            }
        }

        MenuItem {
            text: "Dump current"
            onTriggered: {
                idMenuBar.dumpCurrent()
            }
        }

        MenuItem {
            text: "Clear cache"
            onTriggered: {
                idMenuBar.clearCache()
            }
        }

        MenuItem {
            text: "Quit"
            onTriggered: {
                idMenuBar.quit()
            }
        }
    }
}
