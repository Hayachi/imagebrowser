#ifndef SETTINGSCACHE_H
#define SETTINGSCACHE_H

#include <QQuickItem>
#include <QJsonArray>

class SettingsCache : public QQuickItem
{
    Q_OBJECT
public:
    SettingsCache();
    ~SettingsCache();

    Q_PROPERTY(int filtersCount READ filtersCount NOTIFY filtersCountChanged)
    Q_PROPERTY(QString lastAddress READ lastAddress WRITE setLastAddress NOTIFY lastAddressChanged)
    Q_PROPERTY(int columnsCount READ columnsCount WRITE setColumnsCount NOTIFY columnsCountChanged)
    Q_PROPERTY(int itemsCount READ itemsCount WRITE setItemsCount NOTIFY itemsCountChanged)
    Q_PROPERTY(QString lastRegex READ lastRegex WRITE setLastRegex NOTIFY lastRegexChanged)
    Q_PROPERTY(int lastCapGroup READ lastCapGroup WRITE setLastCapGroup NOTIFY lastCapGroupChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(int thumbnailGroup READ thumbnailGroup WRITE setThumbnailGroup NOTIFY thumbnailGroupChanged)

    Q_INVOKABLE void saveCache();
    Q_INVOKABLE void saveCurrent();
    Q_INVOKABLE void reload();
    Q_INVOKABLE QString patternAt(int);
    Q_INVOKABLE int thumbnailGroupAt(int);
    Q_INVOKABLE int capGroupAt(int);
    Q_INVOKABLE QString nameAt(int);
    Q_INVOKABLE void remove(int);
    Q_INVOKABLE void set(int);
    Q_INVOKABLE void setDefaultSettings();
    Q_INVOKABLE void setDefaultFilters();
    Q_INVOKABLE void clearFilters();
    Q_INVOKABLE void clearSettings();

    int columnsCount() const;
    void setColumnsCount(int);
    int itemsCount() const;
    void setItemsCount(int);
    QString lastAddress() const;
    void setLastAddress(QString);
    int filtersCount() const;
    QString lastRegex() const;
    void setLastRegex(QString);
    int lastCapGroup() const;
    void setLastCapGroup(int);
    QString lastName() const;
    void setLastName(QString);
    int thumbnailGroup() const;
    void setThumbnailGroup(int);

signals:
    void filtersCountChanged();
    void regexChanged();
    void capGroupChanged();
    void nameChanged();
    void columnsCountChanged();
    void lastAddressChanged();
    void itemsCountChanged();
    void lastRegexChanged();
    void lastCapGroupChanged();
    void lastNameChanged();
    void thumbnailGroupChanged();

public slots:

private:
    QString m_cacheFile;
    QJsonArray m_filters;
    int m_columnsCount;
    int m_itemsCount;
    QString m_lastAddress;
    QString m_lastRegex;
    int m_lastCapGroup;
    QString m_lastName;
    int m_thumbnailGroup;
};

#endif // SETTINGSCACHE_H
