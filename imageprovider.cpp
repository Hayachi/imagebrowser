#include "imageprovider.h"
#include <QtNetwork>
#include <QImage>
#include <QStandardPaths>
#include <QDir>

ImageProvider::ImageProvider()
{
    m_address = "";
    m_regexp = "";
    m_scanReply = nullptr;
    m_columns = 6;
    m_maxImagesCount = 30;
    m_imagesCount = 0;
    m_capGroup = 0;
    m_unknownImages = 0;
    m_totalFound = 0;
    m_thumbGroup = -1;
    loadCache();
}

ImageProvider::~ImageProvider()
{
    saveCache();
}

QString ImageProvider::getImage(int id) const
{
    if (m_images.size() <= id) {
        return QString();
    }
    return m_images[id].first;
}

QString ImageProvider::getThumb(int id) const
{
    if (m_images.size() <= id) {
        return QString();
    }
    if (m_images[id].second != "") {
        return m_images[id].second;
    } else {
        return m_images[id].first;
    }
}

void ImageProvider::setAddress(QString a)
{
    m_address = a;
    emit(addressChanged());
}

void ImageProvider::setRegexp(QString r)
{
    m_regexp = r;
    emit(regexpChanged());
}

int ImageProvider::imagesCount() const
{
    return m_imagesCount;
}

void ImageProvider::setImagesCount(int c)
{
    bool hadImg = imagesCount() > 0;
    m_imagesCount = 0;
    emit(imagesCountChanged());
    int newSize = c;

    if (newSize > m_images.size()) {
        newSize = m_images.size();
    }

    if (newSize > m_maxImagesCount) {
        newSize = m_maxImagesCount;
    }

    m_imagesCount = newSize;

    emit(imagesCountChanged());
    if (hadImg != hasImages()) {
        emit(hasImagesChanged());
    }
}

int ImageProvider::maxImagesCount() const
{
    return m_maxImagesCount;
}

void ImageProvider::setMaxImagesCount(int c)
{
    m_maxImagesCount = c;
    emit(maxImagesCountChanged());
}

bool ImageProvider::hasImages() const
{
    return imagesCount() > 0;
}

int ImageProvider::capGroup() const
{
    return m_capGroup;
}

void ImageProvider::setCapGroup(int g)
{
    m_capGroup = g;
    emit(capGroupChanged());
}

int ImageProvider::thumbGroup() const
{
    return m_thumbGroup;
}

void ImageProvider::setThumbGroup(int g)
{
    m_thumbGroup = g;
    emit thumbGroupChanged();
}

int ImageProvider::imagesFound() const
{
    return m_images.size();
}

void ImageProvider::remove(int id)
{
    if (id >= m_images.size()) {
        return;
    }
    m_records[m_images[id].first] = false;
    m_images.remove(id);
    saveCache();
    setImagesCount(maxImagesCount());
    emit imagesFoundChanged();
}

void ImageProvider::save(int id)
{
    if (id >= m_images.size()) {
        return;
    }
    m_records[m_images[id].first] = true;
    QNetworkRequest req;
    req.setUrl(m_images[id].first);
    QNetworkReply *rep = m_manager.get(req);
    connect(rep, &QNetworkReply::finished,
            this, &ImageProvider::parseImage);
    m_imageReplies.push_back({rep, m_images[id].first});
    m_images.remove(id);
    saveCache();
    setImagesCount(maxImagesCount());
    emit imagesFoundChanged();
}

void ImageProvider::popCurrent()
{
    int size = imagesCount();
    for (int i = 0; i < size; ++i) {
        m_records[m_images[size - i - 1].first] = false;
        std::swap(m_images[size - i - 1], m_images.back());
        m_images.pop_back();
    }
    saveCache();
    setImagesCount(maxImagesCount());
    emit imagesFoundChanged();
}

QString ImageProvider::regexp() const
{
    return m_regexp;
}

void ImageProvider::clearCache()
{
    m_records = QJsonObject();
    saveCache();
    reload();
}

void ImageProvider::saveCache()
{
    QDir cachePath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/ImageBrowser/");
    cachePath.mkpath(cachePath.path());
    QFile cache(cachePath.path() + "/record.jsn");
    cache.open(QFile::WriteOnly);
    if (cache.isOpen()) {
        QJsonDocument doc;
        doc.setObject(m_records);
        QByteArray cont = doc.toJson();
        cache.write(cont);
        cache.close();
        qDebug() << "+Image cache saved";
    } else {
        qDebug() << "-Failed to save image cache";
    }
}

void ImageProvider::loadCache()
{
    initialize();
}

int ImageProvider::columns() const
{
    return m_columns;
}

void ImageProvider::setColumns(int c)
{
    m_columns = c;
    emit(columnsChanged());
}

QString ImageProvider::address() const
{
    return m_address;
}

void ImageProvider::parseReply()
{
    qDebug() << "+Reply received";
    if (!m_scanReply) {
        return;
    }
    m_totalFound = 0;
    m_unknownImages = 0;
    QString content(m_scanReply->readAll());
    if (m_regexp != "") {
        QRegExp rx(m_regexp);
        m_images.clear();
        int pos = 0;
        qDebug() << "+Effective regex: " << m_regexp;
        while ((pos = rx.indexIn(content, pos)) != -1) {
            QString name = rx.cap(capGroup());
            QString thumb;
            if (thumbGroup() != -1) {
                thumb = rx.cap(thumbGroup());
            }
            tryPush(name, thumb);
            pos += rx.matchedLength();
        }
    }
    m_scanReply->deleteLater();
    m_scanReply = nullptr;
    qDebug() << "+Images found: " << m_totalFound << ", Unknown: " << m_unknownImages;
    emit imagesFoundChanged();
    setImagesCount(maxImagesCount());
}

void ImageProvider::parseImage()
{
    for (int i = 0; i < m_imageReplies.size(); ++i) {
        if (m_imageReplies[i].first->isFinished()) {
            pareseImageReply(m_imageReplies[i]);
            std::swap(m_imageReplies.back(), m_imageReplies[i]);
            m_imageReplies.pop_back();
            --i;
        }
    }
}

void ImageProvider::initialize()
{
    static bool m_initialized = false;
    if (!m_initialized) {
        m_initialized = true;
        QDir cachePath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/ImageBrowser/");
        cachePath.mkpath(cachePath.path());
        QFile cache(cachePath.path() + "/record.jsn");
        cache.open(QFile::ReadOnly);
        if (cache.isOpen()) {
            QJsonDocument doc;
            QByteArray cont = cache.readAll();
            doc = QJsonDocument::fromJson(cont);
            m_records = doc.object();
            qDebug() << "+Image cache read";
        } else {
            qDebug() << "-No image cache found";
        }
        cache.close();
    }
}

void ImageProvider::reload()
{
    qDebug() << "+Requesting site: " << m_address;
    qDebug() << "+Requested columns: " << m_columns;
    qDebug() << "+Requested items: " << m_maxImagesCount;
    qDebug() << "+Requested regex cap: " << m_capGroup;
    qDebug() << "+Requested thumb cap: " << m_thumbGroup;
    QNetworkRequest req;
    req.setUrl(QUrl(m_address));
    if (m_scanReply) {
        m_scanReply->deleteLater();
    }
    m_scanReply = m_manager.get(req);
    m_scanReply->ignoreSslErrors();
    connect(m_scanReply, &QNetworkReply::finished,
            this, &ImageProvider::parseReply);
}

void ImageProvider::tryPush(QString im, QString th)
{
    ++m_totalFound;
    //qDebug() << "Raw: " << im << " " << th;
    im = fixUrl(im);
    if (th != "") {
        th = fixUrl(th);
    }
    if (m_records.find(im) == m_records.end()) {
        ++m_unknownImages;
        m_images.push_back({im, th});
        //qDebug() << "+Pushing: " << im << " " << th;
    }
}

void ImageProvider::pareseImageReply(QPair<QNetworkReply *, QString> r)
{
    QStringList ls = r.second.split('/');
    QString filename = ls.back();
    QDir cache(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation) + "/ImageBrowser/");
    cache.mkpath(cache.path());
    QFile img(cache.path() + "/" + filename);
    img.open(QFile::WriteOnly);
    if (img.isOpen()) {
        img.write(r.first->readAll());
        img.close();
        qDebug() << "+File saved in: " << cache.path();
    } else {
        qDebug() << "--Failed to open image file: " << cache.path() + "/" + filename;
    }
}

QString ImageProvider::fixUrl(QString u)
{
    QUrl tmpUrl(u);
    QUrl siteUrl(address());
    if (tmpUrl.scheme() == "") {
        tmpUrl.setScheme(siteUrl.scheme());
    }
    if (tmpUrl.host() == "") {
        tmpUrl.setHost(siteUrl.host());
    }
    return tmpUrl.url();
}

