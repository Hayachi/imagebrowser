import QtQuick 2.0

Item {
    id: idContainer
    clip: true

    signal requestSave
    signal requestRemove
    signal clicked

    property bool interactive: false
    property alias source: idDraggable.source

    Rectangle {
        height: parent.height - 4
        width: parent.width / 2
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        color: "#50C050"

        Image {
            anchors.fill: parent
            scale: 0.5
            source: "/res/save-icon.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Rectangle {
        height: parent.height - 4
        width: parent.width / 2
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        color: "#C05050"

        Image {
            anchors.fill: parent
            scale: 0.5
            source: "/res/trash-.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    SwipeArea {
        id: idMouseArea
        anchors.fill: parent
        swipeThreshhold: 5
        onSwipingHorizontalyChanged: {
            idContainer.interactive = swipingHorizontaly
            if (!swipingHorizontaly) {
                idDraggable.moveBack()
            }
        }
        onSwipeHorizontalChanged: {
            idDraggable.x = swipeHorizontal
        }
        onSwipedLeft: {
            idContainer.interactive = false
            idContainer.requestSave()
        }
        onSwipedRight: {
            idContainer.interactive = false
            idContainer.requestRemove()
        }
        onClicked: {
            idContainer.clicked()
        }
    }

    DraggableImage {
        id: idDraggable
        width: parent.width
        height: parent.height
    }
}
