#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include "imageprovider.h"
#include <QStandardPaths>
#include <regex>
#include "settingscache.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qmlRegisterType<ImageProvider>("ImageBrowser", 1, 0, "ImageProvider");
    qmlRegisterType<SettingsCache>("ImageBrowser", 1, 0, "SettingsCache");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
