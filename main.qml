import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import ImageBrowser 1.0

ApplicationWindow {
    id: idWindow
    title: qsTr("ImageBrowser")
    height: 480
    width: 655
    visible: true

    menuBar: SettingsMenuBar {
        onShowSettings: {
            idImageBoard.visible = false
            idSiteOptions.visible = true
        }
        onClearCache: {
            idImageBoard.clearCache()
        }
        onDumpCurrent: {
            idImageBoard.popCurrent()
        }
        onQuit: {
            idWindow.close()
        }
    }

    ImageBoard {
        id: idImageBoard
        anchors.fill: parent
        columns: idSettingsCache.columnsCount
        maxImagesCount: idSettingsCache.itemsCount
        regexp: idSettingsCache.lastRegex
        address: idSettingsCache.lastAddress
        capGroup: idSettingsCache.lastCapGroup
        thumbGroup: idSettingsCache.thumbnailGroup

        onRequestNew: {
            idImageBoard.visible = false
            idSiteOptions.visible = true
        }
    }

    SettingsCache {
        id: idSettingsCache
    }

    Item {
        id: idSiteOptions
        anchors.fill: parent
        anchors.margins: 10
        visible: false

        Column {
            id: idSettingsColumn
            spacing: 4
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            ScrollView {
                id: idScroll
                height: Math.min(idSiteOptions.height * 0.4, idFieldColumn.height);
                anchors.left: parent.left
                anchors.right: parent.right
                flickableItem.flickableDirection: Flickable.VerticalFlick
                horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

                Column {
                    id: idFieldColumn
                    spacing: 4

                    SettingsField {
                        id: idSiteField
                        width: idSiteOptions.width
                        labelText: "Site:"
                        currentText: idSettingsCache.lastAddress
                    }

                    SettingsField {
                        id: idRegexpField
                        width: idSiteOptions.width
                        labelText: "Regexp"
                        currentText: idSettingsCache.lastRegex
                    }

                    SettingsField {
                        id: idColumnsField
                        width: idSiteOptions.width
                        labelText: "Columns:"
                        currentText: idSettingsCache.columnsCount
                    }

                    SettingsField {
                        id: idItemCountField
                        width: idSiteOptions.width
                        labelText: "Items:"
                        currentText: idSettingsCache.itemsCount
                    }

                    SettingsField {
                        id: idCapGroup
                        width: idSiteOptions.width
                        labelText: "Capture group:"
                        currentText: idSettingsCache.lastCapGroup
                    }

                    SettingsField {
                        id: idThumbGroup
                        width: idSiteOptions.width
                        labelText: "Thumbnail group:"
                        currentText: idSettingsCache.thumbnailGroup
                    }

                    SettingsField {
                        id: idNameGroup
                        width: idSiteOptions.width
                        labelText: "Filter name:"
                        currentText: idSettingsCache.lastName
                    }
                }
            }

            Label {
                id: idLabel
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Saved regexps"
            }

            Item {
                id: idSavedFilters
                anchors.horizontalCenter: parent.horizontalCenter
                width: idSiteOptions.width * 0.9
                height: idSiteOptions.height * 0.4 - idButtons.height
                visible: true

                Component {
                    id: idListDelegate
                    Item {
                        id: idWrapper
                        width: idSavedFilters.width
                        height: idListCompText1.height + 2
                        Row {
                            spacing: 4
                            Item {
                                width: idListView.width
                                height: idListCompText3.height + 6
                                Rectangle {
                                    id: idContaierField3
                                    anchors.left: parent.left
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    width: Math.min(parent.width * 0.5, idListCompText3.width + 10)
                                    border.width: 1
                                    radius: 4
                                    color: idWrapper.ListView.isCurrentItem ? "#E0FFE0" : "#E0E0E0"

                                    Text {
                                        anchors.centerIn: parent
                                        id: idListCompText3
                                        visible: true
                                        text: idSettingsCache.nameAt(index)
                                        clip: true
                                    }
                                }
                                Rectangle {
                                    id: idContaierField2
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    anchors.left: idContaierField3.right
                                    anchors.right: idContainerField1.left
                                    border.width: 1
                                    radius: 4
                                    color: idWrapper.ListView.isCurrentItem ? "#E0FFE0" : "#E0E0E0"

                                    Text {
                                        anchors.left: parent.left
                                        anchors.leftMargin: 10
                                        anchors.verticalCenter: parent.verticalCenter
                                        textFormat: Text.PlainText
                                        id: idListCompText2
                                        visible: true
                                        text: idSettingsCache.patternAt(index)
                                        clip: true
                                    }
                                }

                                Rectangle {
                                    id: idContainerField1
                                    width: Math.min(idListView.width * 0.1, idListCompText1.width + 10)
                                    anchors.right: idContainerField0.left
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    border.width: 1
                                    radius: 4
                                    color: idWrapper.ListView.isCurrentItem ? "#E0FFE0" : "#E0E0E0"

                                    Text {
                                        id: idListCompText1
                                        anchors.centerIn: parent
                                        text: idSettingsCache.capGroupAt(index)
                                        clip: true
                                    }
                                }

                                Rectangle {
                                    id: idContainerField0
                                    width: Math.min(idListView.width * 0.1, idListCompText0.width + 10)
                                    anchors.right: parent.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    border.width: 1
                                    radius: 4
                                    color: idWrapper.ListView.isCurrentItem ? "#E0FFE0" : "#E0E0E0"

                                    Text {
                                        id: idListCompText0
                                        anchors.centerIn: parent
                                        text: idSettingsCache.thumbnailGroupAt(index)
                                        clip: true

                                    }
                                }

                            }
                        }
                    }
                }

                ListView {
                    id: idListView
                    anchors.fill: parent
                    contentHeight: idSavedFilters.height
                    contentWidth: idSavedFilters.width
                    spacing: 4
                    model: idSettingsCache.filtersCount

                    visible: true
                    delegate: idListDelegate
                    clip: true
                    focus: true

                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        onClicked: {
                            if (mouse.button == Qt.LeftButton) {
                                var id = idListView.indexAt(mouse.x, mouse.y + idListView.contentY)
                                idListView.currentIndex = id
                                idSettingsCache.set(id)
                                idListView.focus = true
                            } else if (mouse.button == Qt.RightButton) {
                                var id = idListView.indexAt(mouse.x, mouse.y )
                                idSettingsCache.remove(id)
                                idListView.currentIndex = id
                                idListView.focus = true
                            }
                        }
                    }
                }

            }


        }

        Row {
            id: idButtons
            spacing: 4
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            Button {
                text: "Go"
                onClicked: {
                    idSettingsCache.lastRegex = idRegexpField.currentText
                    idSettingsCache.columnsCount = idColumnsField.currentText
                    idSettingsCache.itemsCount = idItemCountField.currentText
                    idSettingsCache.lastCapGroup = idCapGroup.currentText
                    idSettingsCache.lastAddress = idSiteField.currentText
                    idSettingsCache.thumbnailGroup = idThumbGroup.currentText
                    idSettingsCache.saveCache();
                    idSiteOptions.focus = false
                    idSiteOptions.visible = false
                    idImageBoard.visible = true
                    idImageBoard.reload()
                }
            }

            Button {
                text: "Save RegExp"
                onClicked: {
                    idSettingsCache.lastRegex = idRegexpField.currentText
                    idSettingsCache.lastCapGroup = idCapGroup.currentText
                    idSettingsCache.lastName = idNameGroup.currentText
                    idSettingsCache.thumbnailGroup = idThumbGroup.currentText
                    idSettingsCache.saveCurrent()
                    idSettingsCache.saveCache()
                }
            }

            Button {
                text: "Delete selected"
                onClicked: {
                    var id = idListView.currentIndex
                    idSettingsCache.remove(id)
                    if (id == 0) {
                        idListView.currentIndex = 0
                    } else {
                        idListView.currentIndex = id - 1
                    }
                }
            }

            Button {
                text: "Clear filters"
                onClicked: {
                    idSettingsCache.clearFilters()
                }
            }

            Button {
                text: "Clear settings"
                onClicked: {
                    idSettingsCache.clearSettings()
                }
            }
        }
    }

}


