import QtQuick 2.4
import QtQuick.Controls 1.3

Item {
    id: idContainer
    property alias labelText: idLabelText.text
    property alias currentText: idInputText.text
    height: Math.max(idLabel.height, idInputContainer.height, idPasteButton.height)

    Item {
        id: idLabel
        anchors.left: parent.left
        anchors.top: parent.top
        //width: Math.min(idContainer.width / 3, Math.max(idLabelText.width, idContainer.width / 6))
        width: Math.max(idLabelText.width, idContainer.width / 6)
        height: idLabelText.height
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: 2
        clip: true
        Text {
            id: idLabelText
            anchors.centerIn: parent
            text: "Label:"
        }
    }

    Rectangle {
        id: idInputContainer
        anchors.left: idLabel.right
        anchors.right: idPasteButton.left
        anchors.leftMargin: 4
        anchors.rightMargin: 4
        anchors.top: parent.top
        height: idPasteButton.height
        clip: true
        border.width: 1

        TextInput {
            id: idInputText
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 4
            onFocusChanged: {
                cursorVisible = focus
            }
            selectByMouse: true
            width: idContainer.width * 0.8
            autoScroll: true
            text: ""
        }
    }

    Button {
        id: idPasteButton
        text: "Paste"
        anchors.right: parent.right
        anchors.top: parent.top
        onClicked: {
            idInputText.text = ""
            idInputText.paste()
        }
    }
}
